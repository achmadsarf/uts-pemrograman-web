<!DOCTYPE html>
<html>
<head>
	<title> Form Pendaftaran</title>
	<link rel="stylesheet" type="text/css" href="css/stylee.css"> 
</head>
<body>
	<nav class="menu">
    <label>Form Pendaftaran</label>
    <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="pendaftar.php">Data Pendaftar</a></li>
    </ul>
	</nav>
	<div class="wrap"> 
		<h1>Form Pendaftaran Anggota Partai XYZ</h1>
		<h2>Wilayah Jakarta Utara</h2>
		<form action="form_add_aksi.php"method="POST"> 
			<table>
				<tr> 
					<td>Nama Lengkap</td>
					<td>:</td>
					<td><input type="text" name="nama_lengkap"></td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td>:</td>
					<td><textarea name="alamat"></textarea></td>
				</tr>
				<tr>
					<td>Tempat Lahir</td>
					<td>:</td>
					<td><input type="text" name="tempat_lahir"></td>
				</tr>
				<tr>
					<td>Tanggal Lahir</td>
					<td>:</td>
					<td><input type="date" name="tanggal_lahir"></td>
				</tr>
				<tr>
					<td>Jenis Kelamin</td>
					<td>:</td>
					<td>
						<input type="radio" name="jk" value="Laki-Laki">Laki-Laki
						<input type="radio" name="jk" value="perempuan">Perempuan
					</td>
				</tr>
				<tr>
					<td>No Telepon</td>
					<td>:</td>
					<td><input type="number" name="no_telepon"></td>
				</tr>
				<tr>
					<td>Email</td>
					<td>:</td>
					<td><input type="text" name="email"></td>
				</tr>
				<tr>
					<td>Kecamatan</td>
					<td>:</td>
					<td>
						<select name="Kecamatan">
							<option>--Pilih Kecamatan--</option>
							<option>Penjaringan</option>
							<option>Pademangan</option>
							<option>Tanjung Priok</option>
							<option>Koja</option>
							<option>Cilincing</option>
							<option>Kelapa Gading</option>
						</select>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td>
						<button type="submit" name="kirim">Kirim</button>
						<button type="reset" name="reset">Reset</button>
					
					</td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>